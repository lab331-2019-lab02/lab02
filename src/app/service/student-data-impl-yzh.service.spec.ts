import { TestBed, inject } from '@angular/core/testing';

import { StudentDataImplYzhService } from './student-data-impl-yzh.service';

describe('StudentDataImplYzhService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentDataImplYzhService]
    });
  });

  it('should be created', inject([StudentDataImplYzhService], (service: StudentDataImplYzhService) => {
    expect(service).toBeTruthy();
  }));
});
