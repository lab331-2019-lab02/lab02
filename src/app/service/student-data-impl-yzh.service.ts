import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of} from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImplYzhService extends StudentService {

  constructor() {
    super()
  }
  
  getStudents(): Observable<Student[]>{
    return of(this.students)
  };

  students: Student[] = [{
    'id': 1,
    'studentId': '592115521',
    'name': 'Zihao',
    'surname': 'Yu',
    'gpa': 2.73
  }]
}
